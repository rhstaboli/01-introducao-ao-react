import React, { Component } from 'react';
import logo from './hello-world.jpg';
import './App.css';


var text = "Hello, World".split(''),
        n = text.length;

    for(var i = n - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var tmp = text[i];
        text[i] = text[j];
        text[j] = tmp;
    }
const d = new Date();


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <a
            className="App-link"
            href="https://www.google.com/search?q=hello+world&oq=hello+world&aqs=chrome.0.69i59l2j69i60l2j0l2.1774j0j4&sourceid=chrome&ie=UTF-8"
            target="_blank"
            rel="noopener noreferrer"
          >
            Google Pesquisando Hello, World!
          </a>
          <body class="corpo">
          <p>
            <h1>Hello, World!</h1>
            <span>{d.toString()}</span>
            <h1>{"Hello, Wolrd".toUpperCase()}</h1>
            <span>{d.toString().toUpperCase()}</span>
            <h1>{"Hello, Wolrd".toLowerCase()}</h1>
            <span>{d.toString().toLowerCase()}</span>
            <h1>{"Hello, Wolrd".toString().split('').reverse().join('')}</h1>
            <span>{d.toString().split('').reverse().join('')}</span>
            <p class="cabeca_para_baixo"><h1>Hello, World <br/><span>{d.toString()}</span></h1></p>
            <p class="textoVertical"><h1>Hello, World <br/><span>{d.toString()}</span></h1></p>
            <h1>{"Hello, Wolrd".split('').join(' ')}</h1>
            <span>{d.toString().split('').join(' ')}</span>
            <h1><spam class="textoSublinado">Hello, Wolrd <br/><span>{d.toString()}</span> </spam></h1>
            <h1>{text}</h1>
            <h1><spam class="sombra">Hello, Wolrd <br/><span>{d.toString()}</span> </spam></h1>
          </p>
          </body>
        </header>
      </div>
    );
  }
}




export default App;
